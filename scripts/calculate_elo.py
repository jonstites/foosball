#!/usr/bin/env python3


from collections import Counter
import sys

def main(games, start_elo=1000, K=50, F=1000, 
         elo_file_single="data/elo_single.tsv", 
         elo_file_doubles="data/elo_doubles.tsv", 
         elo_file_combined="data/elo_combined.tsv",
         nemesis_file="data/nemesis.tsv"):

    elo_single, elo_doubles, elo_combined, wins_single, wins_doubles, wins_combined, losses_single, losses_doubles, losses_combined, nemesis_single, nemesis_doubles, nemesis_combined = calculate_elo(games, start_elo, K, F)
    with open(elo_file_single, 'w') as elo_handle:
        for person, elo_score in sorted(elo_single.items(), key=lambda x: x[1], reverse=True):
            print(person, "Elo: ", int(elo_score), "wins: ", wins_single[person], "losses: ", losses_single[person], sep="\t", file=elo_handle)


    with open(elo_file_doubles, 'w') as elo_handle:
        for person, elo_score in sorted(elo_doubles.items(), key=lambda x: x[1], reverse=True):
            print(person, "Elo: ", int(elo_score), "wins: ", wins_doubles[person], "losses: ", losses_doubles[person], sep="\t", file=elo_handle)

    with open(elo_file_combined, 'w') as elo_handle:
        for person, elo_score in sorted(elo_combined.items(), key=lambda x: x[1], reverse=True):
            print(person, "Elo: ", int(elo_score), "wins: ", wins_combined[person], "losses: ", losses_combined[person], sep="\t", file=elo_handle)

    with open(nemesis_file, 'w') as f_handle:
        for person, elo_score in sorted(elo_combined.items(), key=lambda x: x[1], reverse=True):
            nem_single_person, nem_single_val = get_nem(nemesis_single, person)
            chump_single_person, chump_single_val = get_nem(nemesis_single, person, chump=True)
            nem_doubles_person, nem_doubles_val = get_nem(nemesis_doubles, person)
            chump_doubles_person, chump_doubles_val = get_nem(nemesis_doubles, person, chump=True)
            nem_combined_person, nem_combined_val = get_nem(nemesis_combined, person)
            chump_combined_person, chump_combined_val = get_nem(nemesis_combined, person, chump=True)
            
            if wins_combined[person] + losses_combined[person] > 10:
                print(person, nem_single_person, nem_single_val, nem_doubles_person, nem_doubles_val, nem_combined_person, nem_combined_val, 
                      chump_single_person, chump_single_val, chump_doubles_person, chump_doubles_val, chump_combined_person, chump_combined_val, file=f_handle)
            

def get_nem(nemesis, person, chump=False):
    if chump:
        try:
            nem_person, nem_val = sorted(nemesis[person].items(), key=lambda x: x[1], reverse=True)[0]
        except KeyError:
            nem_person = None
            nem_val = 0
    else:
        try:
            nem_person, nem_val = sorted(nemesis[person].items(), key=lambda x: x[1])[0]
        except KeyError:
            nem_person = None
            nem_val = 0
    if chump and nem_val <= 0:
        nem_val = None
        nem_person = None
    if not chump and nem_val >= 0:
        nem_val = None
        nem_person = None     
    return nem_person, nem_val


def calculate_elo(games, start_elo, K, F):
    elo_single = {}
    wins_single = Counter()
    losses_single = Counter()

    elo_doubles = {}
    wins_doubles = Counter()
    losses_doubles = Counter()

    elo_combined = {}
    wins_combined = Counter()
    losses_combined = Counter()

    nemesis_single = {}
    nemesis_doubles = {}
    nemesis_combined = {}

    for i, game in enumerate(parse_games(games)):
        date, team1, team2, winner = game
        print("Game number: ", i)
        print(" ".join(team1), " vs ", " ".join(team2))
        print("Winner: ", " ".join(winner))

        if len(team1) != len(team2):
            print("skipping ", team1, team2, file=sys.stderr)
            continue
        
        if len(team1) == 1:
            elo = elo_single
            wins = wins_single
            losses = losses_single
            nem = nemesis_single

        elif len(team1) == 2:
            elo = elo_doubles
            wins = wins_doubles
            losses = losses_doubles
            nem = nemesis_doubles


        update_elo(team1, team2, winner, elo, start_elo, K, F, nem)
        update_wins(winner, wins)
        update_losses(team1, team2, winner, losses)

        update_elo(team1, team2, winner, elo_combined, start_elo, K, F, nemesis_combined)
        update_wins(winner, wins_combined)
        update_losses(team1, team2, winner, losses_combined)

    return elo_single, elo_doubles, elo_combined, wins_single, wins_doubles, wins_combined, losses_single, losses_doubles, losses_combined, nemesis_single, nemesis_doubles, nemesis_combined


def update_wins(winner, wins):
    for person in winner:
        wins[person] += 1

def update_losses(team1, team2, winner, losses):
    for team in (team1, team2):
        if team != winner:
            for person in team:
                losses[person] += 1

def get_team_elo(team, elo, start_elo):
    team_elo = 0
    for person in team:
        team_elo += elo.get(person, start_elo)
    # Return average of player elo on team
    return team_elo/len(team)
#    return team_elo
    
def update_elo(team1, team2, winner, elo, start_elo, K, F, nemesis):
    team1_ro = get_team_elo(team1, elo, start_elo)
    team2_ro = get_team_elo(team2, elo, start_elo)
    S = get_S(team1, team2, winner)
    D = get_D(team1_ro, team2_ro)
    We = get_We(D, F)

    update_amount = K * (S - We)

    print(" ".join(team1), " elo", int(team1_ro))
    print(" ".join(team2), " elo", int(team2_ro))

    # Needs to happen pre-updating
    update_nemesis(nemesis, team1, team2, elo, update_amount, start_elo)
    
    for person in team1:
        print("Elo change for ", person, ":", int(update_amount))
        elo[person] = elo.get(person, start_elo) + update_amount

    for person in team2:
        print("Elo change for ", person, ":", int(-update_amount))
        elo[person] = elo.get(person, start_elo) - update_amount
    print()

def update_nemesis(nemesis, team1, team2, elo, update_amount, start_elo):
    for person in team1:
        nem_person = nemesis.setdefault(person, {})
        # Don't get average, get total enemy elo
        enemy_team_elo = get_team_elo(team2, elo, start_elo)* len(team2)
        for enemy in team2:
            enemy_elo = elo.get(enemy, start_elo) 
            elo_contribution = enemy_elo / enemy_team_elo
            update_contribution = elo_contribution * update_amount
            nemesis[person][enemy] = update_contribution + nem_person.get(enemy, 0)

    for person in team2:
        nem_person = nemesis.setdefault(person, {})
        enemy_team_elo = get_team_elo(team1, elo, start_elo)* len(team1)
        for enemy in team1:
            enemy_elo = elo.get(enemy, start_elo) 
            elo_contribution = enemy_elo / enemy_team_elo
            # Negative because update amount for team2 is -update for team1
            update_contribution = elo_contribution * (update_amount) 
            update_contribution = update_contribution * -1
            nemesis[person][enemy] = update_contribution + nem_person.get(enemy, 0)

def get_D(team1_ro, team2_ro):
    return team1_ro - team2_ro

def get_We(D, F):
    factor = -D / F
    We = 1 / (10 ** factor + 1)
    return We
    

def get_S(team1, team2, winner):
    if team1 == winner:
        return 1
    elif team2 == winner:
        return 0
    else:
        raise Exception(team1, team2, winner)


def parse_games(games):
    with open(games) as game_handle:
        for line in game_handle:
            if "Winner" in line:
                continue
            s = line.split()
            date = s[0]
            team1 = s[1].split(",")
            team2 = s[2].split(",")
            winner = s[3].split(",")
            yield date, team1, team2, winner
            
main("data/foos.tsv")
