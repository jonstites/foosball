#!/usr/bin/env bash

bin/download_foosball_games.sh 
scripts/calculate_elo.py > data/elo_history.txt
scripts/elo_to_html.py
scripts/games_to_html.py
cat data/original_index.erb  data/elo.html data/foos_raw_data.html   > app/views/welcome/index.erb
sed -i 's/Brandon/Big Turbo (Brandon)/g' app/views/welcome/index.erb
sed -i 's/Margot/Hammer (Margot)/g' app/views/welcome/index.erb
sed -i 's/Ed/The Bus (Ed)/' app/views/welcome/index.erb


