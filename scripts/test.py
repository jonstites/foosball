#!/usr/bin/env python3

import json

my_list = [["jon", "chris"], ["brandon"]]

with open("test.txt", 'w') as f:
    json.dump(my_list, fp=f)
