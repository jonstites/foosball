#!/usr/bin/env python3


games = "data/foos.tsv"
output = "data/foos_raw_data.html"

def main(games, output):
    print_html(games, output)


def get_games(games):
    all_games = []
    with open(games) as f:
        next(f)
        for line in f:
            s = line.split()
            date = s[0]
            team1 = s[1].split(",")
            team2 = s[2].split(",")
            winner = s[3].split(",")
            game = date, team1, team2, winner
            all_games.append(game)
    for game in all_games[::-1]:
        yield game


def print_html(games, output_file):
    with open(output_file, 'w') as output:
        print("<h2>Raw Data</h2>", file=output)
        print('<table cellpadding="10">', file=output)
        all_games = get_games(games)
        print('<tr><th>Date</th>', file=output)
        print('<th>Team 1</th>', file=output)
        print('<th>Team 2</th>', file=output)
        print('<th>Winning Team</th>', file=output)
        print('</tr>', file=output)
        for game in get_games(games):
            date, team1, team2, winner = game
            print('<tr>', file=output)
            print('<td>', date, '</td>', file=output)

            if len(team1) > 1:
                team1 = " and ".join(team1)
            else:
                team1 = ''.join(team1)
            if len(team2) > 1:
                team2 = " and ".join(team2)
            else:
                team2 = ''.join(team2)
            if len(winner) > 1:
                winner = " and ".join(winner)
            else:
                winner = ''.join(winner)
            print('<td>', team1, '</td>', file=output)
            print('<td>', team2, '</td>', file=output)
            print('<td>', winner, '</td>', file=output)
            print('</tr>', file=output)
        print("</table", file=output)


if __name__ == "__main__":
    main(games, output)
