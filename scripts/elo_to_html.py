#!/usr/bin/env python3

import datetime


elo_combined = "data/elo_combined.tsv"
elo_single = "data/elo_single.tsv"
elo_doubles = "data/elo_doubles.tsv"
nemesis = "data/nemesis.tsv"
output = "data/elo.html"

def main(elo_combined, elo_single, elo_doubles, nemesis, output):
    with open(output, 'w') as output_handle:
        print_html(elo_combined, output_handle, restricted=True, elo_type="Singles and Doubles")
        print_html(elo_single, output_handle, restricted=True, elo_type="Singles")
        print_html(elo_doubles, output_handle, restricted=True, elo_type="Doubles")
        print_nemesis(nemesis, output_handle)
        print_html(elo_combined, output_handle, elo_type="Singles and Doubles")
        print_html(elo_single, output_handle, elo_type="Singles")
        print_html(elo_doubles, output_handle, elo_type="Doubles")


def get_elo(elo, restricted=False):

    with open(elo) as f:
        rank = 1
        for line in f:
            s = line.split()
            person = s[0]
            elo_score = int(s[2])
            wins = int(s[4])
            losses = int(s[6])
            if restricted:
                if wins + losses < 10:
                    continue
            yield rank, person, elo_score, wins, losses
            rank += 1


def print_html(elo, output, restricted=False, elo_type=None):

    if restricted:
        print("<h2> " + elo_type + " Restricted Rankings (" + u'\u2265' + "10 games)</h2>", file=output)
        print("<h4> Last updated ", datetime.datetime.now().strftime("%Y-%m-%d"), "</h4>", file=output)
    else:
        print("<h2>" + elo_type + " Rankings " + "</h2>", file=output)

    print('<table cellpadding="10">', file=output)
    print('<tr><th>Rank</th>', file=output)
    print('<th>Name</th>', file=output)
    print('<th>Elo</th>', file=output)
    print('<th>Wins</th>', file=output)
    print('<th>Losses</th>', file=output)
    print('<th>Win Percentage</th>', file=output)
    print('</tr>', file=output)
    for rank, person, elo_score, wins, losses in get_elo(elo, restricted):
        win_percent = str(int(round(wins*100 / (wins + losses), 0))) + " %"
        print('<tr>', file=output)
        print('<td>', rank, '</td>', file=output)
        print('<td>', person, '</td>', file=output)
        print('<td>', elo_score, '</td>', file=output)
        print('<td>', wins, '</td>', file=output)
        print('<td>', losses, '</td>', file=output)
        print('<td>', win_percent, '</td>', file=output)
        print('</tr>', file=output)
    print("</table>", file=output)
    print("\n\n", file=output)

def print_nemesis(nemesis, output):
    print("<h2>Nemesis Table</h2>", file=output)
    print('<table cellpadding="10">', file=output)
    print('<tr><th>Person</th>', file=output)
    print('<th>Nemesis (singles)</th>', file=output)
    print('<th>Nemesis (doubles)</th>', file=output)
    print('<th>Nemesis (combined)</th>', file=output)
    print('<th>Patsy (singles)</th>', file=output)
    print('<th>Patsy (doubles)</th>', file=output)
    print('<th>Patsy (combined)</th>', file=output)

    print('</tr>', file=output)
    with open(nemesis) as f:
        for line in f:
            s = line.split()
            person = s[0]
            nem_singles, nem_singles_val = s[1], convert(s[2])
            nem_doubles, nem_doubles_val = s[3], convert(s[4])
            nem_combined, nem_combined_val = s[5], convert(s[6])
            chump_singles, chump_singles_val = s[7], convert(s[8])
            chump_doubles, chump_doubles_val = s[9], convert(s[10])
            chump_combined, chump_combined_val = s[11], convert(s[12])

            nem_s = get_nem(nem_singles, nem_singles_val)
            nem_d = get_nem(nem_doubles, nem_doubles_val)
            nem_c = get_nem(nem_combined, nem_combined_val)

            chump_s = get_nem(chump_singles, chump_singles_val)
            chump_d = get_nem(chump_doubles, chump_doubles_val)
            chump_c = get_nem(chump_combined, chump_combined_val)


            print('<tr>', file=output)
            print("<td>", person, "</td>", file=output)
            print('<td>', nem_s + '</td>', file=output)
            print('<td>', nem_d + '</td>', file=output)
            print('<td>', nem_c + '</td>', file=output)

            print('<td>', chump_s + '</td>', file=output)
            print('<td>', chump_d + '</td>', file=output)
            print('<td>', chump_c + '</td>', file=output)
            print('</tr>', file=output)
    print("</table>", file=output)
    print("\n\n", file=output)

def get_nem(nem, val):
    if nem == "None":
        return nem
    return nem + " " + val + " "

def convert(val):
    if val == "None":
        return val
    else:
        return str(int(float(val)))
if __name__ == "__main__":
    main(elo_combined, elo_single, elo_doubles, nemesis, output)
